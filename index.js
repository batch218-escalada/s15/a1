/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

    let firstName = "John Albert";
    console.log("First Name: " + firstName);

    let lastName = "Escalada";
    console.log("Last Name: " + lastName);

    let Age = 27;
    console.log(Age);

    let Hobbies = ["Playing DOTA 2" , "Playing Basketball" , "Watching HOD"];
    console.log(Hobbies);

    let WorkAddress = {
        houseNumber: 16,
        street: "St. Michael",
        city: "General Santos City",
        state: "South Cotabato"
    };

    console.log(WorkAddress);

/*	

	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let completeName = "Steve Rogers";
	console.log("My full name is: " + completeName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are:" + friends)
	console.log(friends);

	let profile = {

		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	};

	console.log("My Full Profile:" + profile);
	console.log(profile);

	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);